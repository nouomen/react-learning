import React, { Component } from 'react'
import { CircularProgress } from '@material-ui/core';

const LoadingHOC = (props) => (WrappedComponent) => {
    return class LoadingHOC extends Component {
        render() {
            return props.itemsLoading ||!props.items.data ? <CircularProgress/> : <WrappedComponent data={props.items.data} />;
        }
    }
};

export default LoadingHOC;