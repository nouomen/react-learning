import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setLogin } from '../actions/login';
import NavBar from '../components/NavBar';
import LoginForm from '../components/LoginForm';
import { CircularProgress } from '@material-ui/core';
import cookie from 'react-cookies';

class Login extends Component {

    state = {
        name: '',
        pass: '',
    };

    onChange = () => {
        this.props.onLogin(this.state);

    };

    updateName = (name) => {
        this.setState({ name: name });
    };

    updatePassword = (pass) => {
        this.setState({ pass: pass });
    };

    render() {
        let is_login = cookie.load('admin') === 'true' || this.props.loginIsAllowed === true;
        return <div>
                <NavBar login={is_login}/>
                {this.props.loginIsChecking ? <CircularProgress/> : null}
                {this.props.loginIsAllowed === false ? <div>Не верный пароль или логин</div> : null}
                {is_login ? <div>Выход Admin</div> : <LoginForm updatePassword={this.updatePassword} updateName={this.updateName} onClick={this.onChange}/>}
            </div>
        ;
    }
}

const mapStateToProps = (state) => {
    return {
        loginIsAllowed: state.loginIsAllowed,
        loginIsChecking: state.loginIsChecking
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (data) => dispatch(setLogin(data)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);