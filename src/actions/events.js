import axi from 'axios';
import * as constants from "../constants";

export function itemsError(bool) {
    return {
        type: 'ITEMS_ERROR',
        hasError: bool
    };
}

export function itemsLoading(bool) {
    return {
        type: 'ITEMS_LOADING',
        itemsLoading: bool
    };
}

export function itemsFetchSuccess(items) {
    return {
        type: 'ITEMS_FETCH_SUCCESS',
        items
    };
}

export function itemsFetchData(page, date = null) {
    const {API_URL} = constants;

    return (dispatch) => {
        dispatch(itemsLoading(true));
        (async () => {
            try {
                let date_str = date ? '&date=' + date : '';
                let response = await axi.get(API_URL + '/articles?page=' + page + date_str);
                dispatch(itemsLoading(false));
                dispatch(itemsFetchSuccess(response.data));
            } catch (e) {
                console.log(e);
                alert(e);
                dispatch(itemsError(true));
            }
        })();
    };
}