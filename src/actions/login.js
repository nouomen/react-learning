import axi from 'axios';
import * as constants from "../constants";
import cookie from 'react-cookies'

export function loginError(bool) {
    return {
        type: 'LOGIN_ERROR',
        hasError: bool
    };
}

export function loginIsChecking(bool) {
    return {
        type: 'LOGIN_IS_CHECKING',
        loginIsChecking: bool
    };
}

export function loginIsAllowed(login) {
    return {
        type: 'LOGIN_IS_ALLOWED',
        login
    };
}

export function setLogin(data) {
    const {API_URL} = constants;

    return (dispatch) => {
        dispatch(loginIsChecking(true));
        (async () => {
            try {
                let response = await axi.post(API_URL + '/login', data);
                dispatch(loginIsChecking(false));
                dispatch(loginIsAllowed(response.data));
                cookie.save('admin', response.data, { path: '/' })
            } catch (e) {
                console.log(e);
                alert(e);
                dispatch(loginError(true));
            }
        })();
    };
}