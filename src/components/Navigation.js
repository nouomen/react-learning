import React from 'react';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';

export default (props) => {
    return <div {...props}>
        <Button><NavLink exact activeStyle={{color: 'white'}} to="/">Афиша</NavLink></Button>
        <Button><NavLink exact activeStyle={{color: 'white'}} to="/about">О нас</NavLink></Button>
        <Button><NavLink exact activeStyle={{color: 'white'}} to="/login">Логин</NavLink></Button>
    </div>
};