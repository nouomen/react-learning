import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
});

const TextFields = (props) => {
    console.log(props);

    const { classes } = props;
    return(
        <form className={classes.container} noValidate autoComplete="off">
            <TextField
                onChange={(event) => {props.updateName(event.target.value)}}
                id="name"
                label="Name"
                className={classes.textField}
                margin="normal"
            />
            <TextField
                onChange={(event) => {props.updatePassword(event.target.value)}}
                id="password"
                label="Password"
                className={classes.textField}
                type="password"
                autoComplete="current-password"
                margin="normal"
            />
            <Button onClick={props.onClick} variant="contained" color="primary" className={classes.button}>Login</Button>
        </form>
    )
};

TextFields.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFields);
