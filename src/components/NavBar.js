import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Navigation from './Navigation';
import '../index.css';
const NavBar = (props) => {
    return(
        <div>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="title" color="inherit">
                        <Navigation property="navigation"/>
                    </Typography>
                    { props.login ? <Button color="inherit" style={{position: 'absolute', right: '10px'}} >Admin (Logout)</Button> : null }
                </Toolbar>
            </AppBar>
        </div>
    )
};
export default NavBar;