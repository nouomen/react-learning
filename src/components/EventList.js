import React from 'react';
import { List, ListItem, ListItemText } from '@material-ui/core';

export default function EventList({data}) {
    const listItem = data.map( ({id, title}) =>
        <ListItem key={id}>
            <ListItemText primary={title}/>
        </ListItem>
    );
    return <List>{listItem}</List>
}