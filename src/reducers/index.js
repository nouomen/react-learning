import { combineReducers } from 'redux';
import { items, itemsError, itemsLoading } from './events';
import { loginIsAllowed, loginError, loginIsChecking } from './login';

// соединяет редьюсеры
export default combineReducers({
    items,
    itemsError,
    itemsLoading,
    loginError,
    loginIsChecking,
    loginIsAllowed,
});