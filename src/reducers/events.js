// это чистая функция, которая меняет store.
export function itemsError(state = false, action) {
    switch (action.type) {
        case 'ITEMS_ERROR':
            return action.hasError;

        default:
            return state;
    }
}

export function itemsLoading(state = false, action) {
    switch (action.type) {
        case 'ITEMS_LOADING':
            return action.itemsLoading;

        default:
            return state;
    }
}

export function items(state = [], action) {
    switch (action.type) {
        case 'ITEMS_FETCH_SUCCESS':
            return action.items;

        default:
            return state;
    }
}