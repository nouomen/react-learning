// это чистая функция, которая меняет store.
export function loginError(state = false, action) {
    switch (action.type) {
        case 'LOGIN_ERROR':
            return action.hasError;

        default:
            return state;
    }
}

export function loginIsChecking(state = false, action) {
    switch (action.type) {
        case 'LOGIN_IS_CHECKING':
            return action.loginIsChecking;

        default:
            return state;
    }
}

export function loginIsAllowed(state = [], action) {
    switch (action.type) {
        case 'LOGIN_IS_ALLOWED':
            return action.login;

        default:
            return state;
    }
}