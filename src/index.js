import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import About  from './components/About';
import Login  from './containers/Login';
import { MuiThemeProvider } from '@material-ui/core/styles';
import blue  from '@material-ui/core/colors/blue';
import { createMuiTheme } from '@material-ui/core/styles'

// 2й параметр нужен для работы плагина в chrome
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

const theme = createMuiTheme({
    palette: {
        primary: blue
    }
});

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path="/" exact render={props => <MuiThemeProvider theme={theme}><App/></MuiThemeProvider>} />
                <Route path="/about" render={props => <MuiThemeProvider theme={theme}><About {...props} /></MuiThemeProvider>} />
                <Route path="/login" render={props => <MuiThemeProvider theme={theme}><Login {...props} /></MuiThemeProvider>} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
