import React, { Component } from 'react';
import { connect } from 'react-redux';
import { itemsFetchData } from './actions/events';
import EventList from './components/EventList';
import LoadingHOC from './hoc/LoadingHOC';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
import {DateFormatInput} from 'material-ui-next-pickers';
import moment from 'moment';
import NavBar from './components/NavBar';
import cookie from 'react-cookies';

class App extends Component {
    // текущая страница пагинации
    state = {
        current: 1,
        admin: false
    };

    componentDidMount() {
        this.props.fetchData(1);
        this.setState({ admin: cookie.load('admin') })
    }

    /**
     * Каледарь
     * @param date
     */
    onChangeDate = (date) => {
        let page = this.state.current;
        let normal_date = date ? moment(date).format('YYYY-MM-DD') : null;
        this.props.fetchData(page, normal_date);
        this.setState({date});
    };

    /**
     * Пагинация
     * @param page
     */
    onChangePagination = (page) => {
        this.props.fetchData(page);
        this.setState({
            current: page,
        });
    };

    render() {
        const {date} = this.state;
        const LoadedEventList = LoadingHOC(this.props)(EventList);
        const total_pages = this.props.items.meta ? this.props.items.meta.total : 0;
        return (
            <div>
                <NavBar login={cookie.load('admin')}/>
                <DateFormatInput name='date-input' value={date} onChange={this.onChangeDate}/>
                <LoadedEventList data={this.props.items.data}/>
                <Pagination onChange={this.onChangePagination} current={this.state.current} total={total_pages} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items,
        itemsLoading: state.itemsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (page, date = null) => dispatch(itemsFetchData(page, date)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);